<?php
if ($method === 'GET') {
    session_start();
    if (isset($_SESSION['user_id'])) {
        $userId = $_SESSION['user_id'];

        // Query the database to retrieve the session ID associated with the user
        $stmt = $this->sqlConn->prepare("SELECT session_id FROM $this->users WHERE user_id = ?");
        $stmt->bind_param("i", $userId);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $dbSessionId = $row['session_id'];

            // Validate the session ID
            if ($_SESSION['session_id'] === $dbSessionId) {
                return ['sessionValid' => true];
            }
        }
    }

    return ['sessionValid' => false];
}
?>
<?php
session_start();

header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');

if (isset($_SESSION['email'])) {
    include 'config.php';
    $id = $_SESSION['email'];
    $sql = "SELECT * FROM `admin` WHERE email = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows == 1) {
        $row = $result->fetch_assoc();
        $firstname = $row['firstname'];
        $lastname = $row['lastname'];
        $user_id = $row['id'];
        $phone = $row['phone'];
        $email = $row['email'];
        $sql2 = "SELECT * FROM `dashboard` WHERE id = ?";
        $d_id = 1;
        $stmt2 = $conn->prepare($sql2);
        $stmt2->bind_param("i", $d_id);
        $stmt2->execute();
        $result2 = $stmt2->get_result();

        if ($result2->num_rows > 0) {
            $row2 = $result2->fetch_assoc();
            $total_balance = $row2['total_balance'];
            $total_income = $row2['total_income'];
            $total_paidout = $row2['total_paidout'];
            $other_expenses = $row2['other_expenses'];
            $total_users = $row2['total_users'];
            $userDetails = array(
                'total_balance' => $total_balance,
                'total_income' => $total_income,
                'total_paidout' => $total_paidout,
                'other_expenses' => $other_expenses,
                'total_users' => $total_users,
                'user_id' => $user_id,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'phone' => $phone,
                'email' => $email,
                'id' => $user_id,
            );

            header('Content-Type: application/json');
            echo json_encode($userDetails);
            exit;
        } else {
            echo json_encode(array('error' => 'dashboard id not found'));
            exit;
        }
    } else {
        echo json_encode(array('error' => 'user id not found'));
        exit;
    }
} else {
    echo json_encode(array('error' => 'Not authorized'));
    exit;
}
?>

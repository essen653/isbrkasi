<?php
    $servername = 'localhost';
    $username = 'root';
    $password = '';
    $dbname = 'student';

    $conn = new mysqli($servername, $username, $password, $dbname);
    if($conn->connect_error) {
        die('Connection Failed, try again' . $conn->connect_error);
    }

?>
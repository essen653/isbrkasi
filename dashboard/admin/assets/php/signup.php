<?php
    $servername = 'localhost';
    $username = 'root';
    $password = '';
    $dbname = 'users';

    $con = mysqli_connect($servername, $username, $password, $dbname);
    if(mysqli_connect_error()) {
        exit('Connection Failed, try again' . mysqli_connect_error());
    }

    if(!isset($_POST['username'], $_POST['password'], $_POST['email'], $_POST['fullname'], $_POST['gender'])) {
        exit('Empty Field(s)');
    }

    if(empty($_POST['username'] || empty($_POST['password']) || empty($_POST['email']) || empty($_POST['gender']) || empty($_POST['fullname']))) {
        exit('Values Empty');
    }

    if($stmt = $con->prepare('SELECT id, password FROM user WHERE username = ?')) {
        $stmt->bind_param('s', $_POST['username']);
        $stmt->execute();
        $stmt->store_result();

        if($stmt->num_rows>0) {
            header("Location: ../../signup.html?error=Username Already Exits");
        }
        // elseif($stmt = $con->prepare('SELECT id, password FROM user WHERE email = ?')) {
        //     $stmt->bind_param('s', $_POST['email']);
        //     $stmt->execute();
        //     $stmt->store_result();
    
        //     if($stmt->num_rows>0) {
        //         echo 'Email Already Exits';
        //     }
        // }
        else {
            if($stmt = $con->prepare('INSERT INTO user (fullname, username, email, gender, password) VALUE (?, ?, ?, ?, ?)')) {
                $pasword = password_hash($_POST['password'], PASSWORD_DEFAULT);
                $stmt->bind_param('sssss', $_POST['fullname'], $_POST['username'], $_POST['email'], $_POST['gender'], $_POST['password']);
                $stmt->execute();
                // echo 'Succesfully Registered';
                header("Location: ../../login.html?success=Registration Succesful");
            }
            else {
                echo 'Error Occurred';
            }
        }
        $stmt->close();
    }
    else {
        echo 'Error Occurred';
    } 
    $con->close();
?>
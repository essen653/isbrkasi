<?php
    $servername = 'localhost';
    $username = 'root';
    $password = '';
    $dbname = 'student';

    $con = mysqli_connect($servername, $username, $password, $dbname);
    session_start();

        if(isset($_SESSION['id']) && isset($_SESSION['username'])) {
            $response = array();
            if ($con) {
                $sql = "SELECT * FROM `student_info`";
                $result = mysqli_query($con, $sql);
                if($result) {
                    $x =0;
                    while ($row = mysqli_fetch_assoc($result)) {
                    $response[$x]['id'] = $row['id'];
                    $response[$x]['reg_no'] = $row['reg_no'];
                    $response[$x]['firstname'] = $row['firstname'];
                    $response[$x]['lastname'] = $row['lastname'];
                    $response[$x]['faculty'] = $row['faculty'];
                    $response[$x]['state'] = $row['state'];
                    $response[$x]['gender'] = $row['gender'];
                    $response[$x]['admitted'] = $row['admitted'];
                    $x++;
                    }
                    echo json_encode($response, JSON_PRETTY_PRINT);
                }
                else {
                    echo "No data found";
                }
            }
            else {
                echo "Database Connection failed.";
            }
        }
        else {
            header("Location: ../../login.html");
            exit();
        }
    ?>
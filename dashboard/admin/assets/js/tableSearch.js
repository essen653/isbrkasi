function search() {
    // Declare variables
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("searchInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("approved");
    tr = table.getElementsByTagName("tr");
  
    for (i = 1; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td");
    var matched = false; // Flag to indicate if any column matches the search query

    for (var j = 1; j < td.length; j++) { // Start from index 1 to skip checkbox column
        var column = td[j];
        if (column) {
        txtValue = column.textContent || column.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            matched = true;
            break; // Break the loop if a match is found in any column
        }
        }
    }

    if (matched) {
        tr[i].style.display = "";
    } else {
        tr[i].style.display = "none";
    }
    }
}
  
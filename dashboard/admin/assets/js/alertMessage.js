// Get the URL parameter named
const urlParams = new URLSearchParams(window.location.search);
const successMsg = urlParams.get('success');
const errorMsg = urlParams.get('error');

// If there is a success message, display it
if (successMsg) {
  document.getElementById('successMessage').innerHTML = successMsg;
  document.getElementById("successMessage").style.display = "block";
}

// If there is an error message, display it
if (errorMsg) {
  document.getElementById('errorMessage').innerHTML = errorMsg;
  document.getElementById("errorMessage").style.display = "block";
}



let sessionID = window.localStorage.getItem("id");
fetch("/../../student/admin/assets/php/studentAPI.php?sessionID="+sessionID)

.then(function(response){
    return response.json();
})
.then(function(students){
    let placeholder = document.querySelector("#admitted");
    let placeholder2 = document.querySelector("#pending");
    let placeholder3 = document.querySelector("#rejected");

    let admitteD = "";
    let pendinG = "";
    let rejecteD = "";
    for(let student of students) {
        if (student.admitted == 1) {
            admitteD += `
            <tr>
            <td>${student.reg_no}</td>
            <td>${student.firstname}  ${student.lastname}</td>
            <td>${student.faculty}</td>
            <td>${student.state}</td>
            <td>${student.gender}</td>
            <td>Admitted</td>
            </tr>
        `;
        }
        else if (student.admitted == 0) {
            pendinG += `
            <tr>
            <td>${student.reg_no}</td>
            <td>${student.firstname}  ${student.lastname}</td>
            <td>${student.faculty}</td>
            <td>${student.state}</td>
            <td>Pending</td>
            <td>
            <div class="btn-group-vertical" role="group" aria-label="Basic example">
            <div class="btn-group">
              <button type="button" class="btn btn-info dropdown-toggle" data-bs-toggle="dropdown">Action</button>
              <div class="dropdown-menu">
                <a class="dropdown-item btn-success" href="/../../student/admin/assets/php/admit.php?id=${student.id}">Admit</a>
                <a class="dropdown-item" href="/../../student/admin/assets/php/pending.php?id=${student.id}">Pend</a>
                <a class="dropdown-item" href="/../../student/admin/assets/php/reject.php?id=${student.id}">Reject</a>
                <a class="dropdown-item" href="/../../student/admin/assets/php/delete.php?id=${student.id}">Delete</a>
              </div>
            </div>
          </div>

            </td>
            </tr>
        `;
        }
        else {
            rejecteD += `
            <tr>
            <td>${student.reg_no}</td>
            <td>${student.firstname}  ${student.lastname}</td>
            <td>${student.faculty}</td>
            <td>${student.state}</td>
            <td>${student.gender}</td>
            <td>Rejected</td>
            </tr>
        `;
        }
        
    }

    if (placeholder !== null) {
      placeholder.innerHTML = admitteD;
    }
    
    if (placeholder2 !== null) {
      placeholder2.innerHTML = pendinG;
    }

    if (placeholder3 !== null) {
      placeholder3.innerHTML = rejecteD;
    }
})

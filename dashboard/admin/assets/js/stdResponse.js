fetch("/../../student/admin/assets/php/studentAPI.php")

.then(function(response){
    return response.json();
})
.then(function(students){
    let placeholder = document.querySelector("#studenT");
    let Y = "Admitted"
    let N = "Pending"
    let R = "Rejected"

    let out = "";
    for(let student of students) {
        if (student.admitted == 0) {
            out += `
            <tr>
            <td>${student.reg_no}</td>
            <td>${student.firstname}  ${student.lastname}</td>
            <td>${student.faculty}</td>
            <td>${student.state}</td>
            <td>${N}</td>
            <td>
            <div class="btn-group-vertical" role="group" aria-label="Basic example">
                            <div class="btn-group">
                              <button type="button" class="btn btn-info dropdown-toggle" data-bs-toggle="dropdown">Action</button>
                              <div class="dropdown-menu">
                                <a class="dropdown-item btn-success" href="/../../student/admin/assets/php/admit.php?id=${student.id}">Admit</a>
                                <a class="dropdown-item" href="/../../student/admin/assets/php/pending.php?id=${student.id}">Pend</a>
                                <a class="dropdown-item" href="/../../student/admin/assets/php/reject.php?id=${student.id}">Reject</a>
                                <a class="dropdown-item" href="/../../student/admin/assets/php/delete.php?id=${student.id}">Delete</a>
                              </div>
                            </div>
                          </div>
                          </td>
            </tr>
        `;
        }
        else if (student.admitted == 1) {
            out += `
            <tr>
            <td>${student.reg_no}</td>
            <td>${student.firstname}  ${student.lastname}</td>
            <td>${student.faculty}</td>
            <td>${student.state}</td>
            <td>${Y}</td>
            <td>
                        <div class="btn-group-vertical" role="group" aria-label="Basic example">
                            <div class="btn-group">
                              <button type="button" class="btn btn-info dropdown-toggle" data-bs-toggle="dropdown">Action</button>
                              <div class="dropdown-menu">
                              <a class="dropdown-item" href="/../../student/admin/assets/php/admit.php?id=${student.id}">Admit</a>
                              <a class="dropdown-item" href="/../../student/admin/assets/php/pending.php?id=${student.id}">Pend</a>
                              <a class="dropdown-item" href="/../../student/admin/assets/php/reject.php?id=${student.id}">Reject</a>
                              <a class="dropdown-item" href="/../../student/admin/assets/php/delete.php?id=${student.id}">Delete</a>
                              </div>
                            </div>
                          </div>
            </tr>
        `;
        } else {
            out += `
            <tr>
            <td>${student.reg_no}</td>
            <td>${student.firstname}  ${student.lastname}</td>
            <td>${student.faculty}</td>
            <td>${student.state}</td>
            <td>${R}</td>
            <td> 
            <div class="btn-group-vertical" role="group" aria-label="Basic example">
            <div class="btn-group">
              <button type="button" class="btn btn-info dropdown-toggle" data-bs-toggle="dropdown">Action</button>
              <div class="dropdown-menu">
              <a class="dropdown-item" href="/../../student/admin/assets/php/admit.php?id=${student.id}">Admit</a>
              <a class="dropdown-item" href="/../../student/admin/assets/php/pending.php?id=${student.id}">Pend</a>
              <a class="dropdown-item" href="/../../student/admin/assets/php/reject.php?id=${student.id}">Reject</a>
              <a class="dropdown-item" href="/../../student/admin/assets/php/delete.php?id=${student.id}">Delete</a>
              </div>
            </div>
          </div>
            
            </td>
            </tr>
        `;
        }
        
    }

    placeholder.innerHTML = out;
})

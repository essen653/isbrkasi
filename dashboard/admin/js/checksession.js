function getQueryParam(param) {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    return urlParams.get(param);
}

// Extract the session ID from the URL
const sessionID = getQueryParam('sessionid');

if (!sessionID) {
    // Session ID is missing, redirect to login page
    window.location.href = '../index.html';
} else {
    // Make a GET request to the server to check for the session
    fetch('../php/checkSession', {
        method: 'GET',
    })
    .then(response => response.json())
    .then(data => {
        console.log('Session Check Response:', data);

        if (data && data.sessionValid === false) {
            // Session is not valid, redirect to login page
            window.location.href = 'dashboard.html';
        }
    })
    .catch(error => {
        console.error('Error:', error);
        // Handle error and potentially redirect to login page
        window.location.href = '../index.html';
    });
}
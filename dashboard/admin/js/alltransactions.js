$(document).ready(function() {
    var rowsPerP = 10;
    var currentPage = 1;  
    function showRows(rowsPerP, currentPage) {
      var rows = $("#history tbody tr");
      var startIndex = (currentPage - 1) * rowsPerP;
      var endIndex = startIndex + rowsPerP;
      rows.hide();
      rows.slice(startIndex, endIndex).show();
    }

    function updateDashboard(data) { 
            const phoneElement = document.getElementById('phone');
            if (phoneElement) {
                phoneElement.value = data.phone;
                phoneElement.textContent = data.phone;
            }
            const idElement = document.getElementById('id');
            if (idElement) {
                idElement.value = data.id;
                idElement.textContent = data.id;
            }
            const firstnameElement = document.getElementById('firstname');
            if (firstnameElement) {
                firstnameElement.textContent = data.firstname;
                firstnameElement.value = data.firstname;
            }
            const lastnameElement = document.getElementById('lastname');
            if (lastnameElement) {
                lastnameElement.textContent = data.lastname;
                lastnameElement.value = data.lastname;
            }
            const balanceElement = document.getElementById('balance');
            if (balanceElement) {
                balanceElement.textContent = data.total_balance; 
            }
            const incomeElement = document.getElementById('income');
            if (incomeElement) {
                incomeElement.textContent = data.total_income; 
            }
            const paidoutElement = document.getElementById('paidout');
            if (paidoutElement) {
                paidoutElement.textContent = data.total_paidout; 
            }
            const expensesElement = document.getElementById('expenses');
            if (expensesElement) {
                expensesElement.textContent = data.other_expenses; 
            }
            const usersElement = document.getElementById('users');
            if (usersElement) {
                usersElement.textContent = data.total_users; 
            }
            const fullnameElement = document.getElementById('fullname');
            if (fullnameElement) {
                fullnameElement.textContent = data.firstname + " " + data.lastname;
                fullnameElement.value = data.firstname + " " + data.lastname;
            }
            const fullname2Element = document.getElementById('fullname2');
            if (fullname2Element) {
                fullname2Element.textContent = data.firstname + " " + data.lastname;
                fullname2Element.value = data.firstname + " " + data.lastname;
            }
            const emailElement = document.getElementById('email');
            if (emailElement) {
                emailElement.textContent = data.email;
                emailElement.value = data.email;
            }
            const email2Element = document.getElementById('email2');
            if (email2Element) {
                email2Element.textContent = data.email;
                email2Element.value = data.email;
            }
            const bankElement = document.getElementById('bank');
            if (bankElement) {
                bankElement.textContent = data.bank;
            }
            const a_numberElement = document.getElementById('a_number');
            if (a_numberElement) {
                a_numberElement.textContent = data.a_number;
            }
            const active_machineElement = document.getElementById('active_machine');
            if (active_machineElement) {
                active_machineElement.textContent = data.active_machine;
            }
            const total_withdrawerElement = document.getElementById('total_withdrawer');
            if (total_withdrawerElement) {
                total_withdrawerElement.textContent = data.total_withdrawer;
            }
    }

    function initializePagination(rows) {
        var numPages = Math.ceil(rows.length / rowsPerP);
        var pagination = $(".pagination");
        function updatePagination() {
            pagination.empty();
            var prevButton = $("<li class='page-item'><a class='page-link' href='#'><span aria-hidden='true'>&laquo;</span></a></li>");
            pagination.append(prevButton);

            var prevEllipsis = $("<li class='page-item disabled'><span class='page-link'>&hellip;</span></li>");

            for (var i = 1; i <= numPages; i++) {
                if (i === currentPage) {
                    var pageLink = $("<li class='page-item active'><span class='page-link'>" + i + "</span></li>");
                } else if (i <= 2 || i >= numPages - 1 || (i >= currentPage - 1 && i <= currentPage + 1)) {
                    var pageLink = $("<li class='page-item'><a class='page-link' href='#'>" + i + "</a></li>");
                } else {
                    if (!prevEllipsis.hasClass('disabled')) {
                        pagination.append(prevEllipsis);
                        prevEllipsis = $("<li class='page-item disabled'><span class='page-link'>&hellip;</span></li>");
                    }
                    continue;
                }

                if (i <= 2 || i >= numPages - 1 || (i >= currentPage - 1 && i <= currentPage + 1)) {
                    pagination.append(pageLink);
                } else {
                    prevEllipsis = $("<li class='page-item disabled'><span class='page-link'>&hellip;</span></li>");
                }
            }
            var nextButton = $("<li class='page-item'><a class='page-link' href='#'><span aria-hidden='true'>&raquo;</span></a></li>");
            pagination.append(nextButton);

            showRows(rowsPerP, currentPage);
        }

        updatePagination();

        pagination.on("click", ".page-link", function (e) {
            e.preventDefault();
            var clickedPage = $(this).text();

            if (clickedPage === "«") {
                currentPage = Math.max(currentPage - 1, 1);
            } else if (clickedPage === "»") {
                currentPage = Math.min(currentPage + 1, numPages);
            } else {
                currentPage = parseInt(clickedPage);
            }
            updatePagination();
        });
    }
  
    function fetchuserTransactionsAndusers() {
      fetch('php/session_admin.php')
        .then(response => response.json())
        .then(data => {
          updateDashboard(data);
          fetchuserTransactions(data.id, initializePagination);
        })
        .catch(error => console.error('Error:', error))
        .finally(() => {
          setTimeout(fetchuserTransactionsAndusers, 90000);
        });
    }

    function search() {
        var input, filter, tableBody, tr, td, i, j, txtValue;
        input = document.getElementById("searchInput");
        filter = input.value.toUpperCase();
        tableBody = document.getElementById("history").getElementsByTagName("tbody")[0];
        tr = tableBody.getElementsByTagName("tr");
    
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td");
            var matched = false; // Flag to indicate if any column matches the search query
    
            for (j = 0; j < td.length; j++) {
                var column = td[j];
                if (column) {
                    txtValue = column.textContent || column.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        matched = true;
                        break; // Break the loop if a match is found in any column
                    }
                }
            }
    
            if (matched) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
  
    function fetchuserTransactions(callback) {
      fetch(`php/admin.php/transactions`)
        .then(response => response.json())
        .then(users => {
          const tableBody = document.querySelector('#history tbody');
          if (users.length > 0) {
            if (tableBody) {
                tableBody.innerHTML = ''; 

                users.reverse();

                users.forEach(useR => {
                    const approved = 'Aprroved';
                    const pending = 'Pending';
                    const rejected = 'Rejected';
                    let result = '';
                    let color = '';

                    if (useR.status === '0') {
                        result = pending;
                        color = 'yellow';
                    } else if (useR.status === '1') {
                        result = approved;
                        color = 'green';
                    } else {
                        result = rejected;
                        color = 'red';
                    }
                    const row = document.createElement('tr');

                    const idCell = document.createElement('td');
                    const id = document.createElement('span');
                    id.textContent = useR.ref_no;
                    idCell.appendChild(id);

                    const userIdCell = document.createElement('td');
                    const userId = document.createElement('span');
                    userId.textContent = useR.user_id;
                    userIdCell.appendChild(userId);

                    const amountCell = document.createElement('td');
                    const amountText = document.createElement('strong');
                    amountText.textContent = useR.amount;
                    amountCell.appendChild(amountText);

                    const desCell = document.createElement('td');
                    const des = document.createElement('span');
                    des.textContent = useR.description;
                    desCell.appendChild(des);

                    const machineCell = document.createElement('td');
                    const machine = document.createElement('span');
                    machine.style.color = color;
                    machine.textContent = result;
                    machineCell.appendChild(machine);

                    const dateCell = document.createElement('td');
                    const dateText = document.createElement('strong');
                    dateText.textContent = useR.date;
                    dateCell.appendChild(dateText);
                    
                    

                    row.appendChild(idCell);
                    row.appendChild(userIdCell);
                    row.appendChild(amountCell);
                    row.appendChild(desCell);
                    row.appendChild(machineCell);
                    row.appendChild(dateCell);

                    tableBody.appendChild(row);
                });
                initializeSearch();
                initializeDropdowns();
            }
            } else {
                tableBody.innerHTML = '<tr><td colspan="2">No users available.</td></tr>';
            }
  
          if (callback) {
            callback(tableBody.children);
          }
        })
        .catch(error => console.error('Error:', error));
    }

    function initializeDropdowns() {
        // Initialize Bootstrap dropdowns for dynamically created elements
        const dropdownToggleElements = document.querySelectorAll('.dropdown-toggle');
        dropdownToggleElements.forEach(dropdownToggle => {
            new bootstrap.Dropdown(dropdownToggle);
        });
    }

    function initializeSearch() {
        const searchInput = document.getElementById("searchInput");
        searchInput.addEventListener("input", search);
    }
    


  
    fetchuserTransactionsAndusers();
  });
  
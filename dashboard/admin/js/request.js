fetch("../admin/php/admin.php/fetchrequest")

.then(function(response){
    return response.json();
})
.then(function(users){
    let placeholder = document.querySelector("#allUsers");

    let out = "";
    for(let user of users) {
        out += `
            <tr>
                <td>${user.amount}</td>
                <td>${user.bank_name}</td>
                <td>${user.account_name}</td>
                <td>${user.account_number}</td>
                <td>${user.email}</td>
                <td>
                    <div class="btn-group-vertical" role="group" aria-label="Basic example">
                        <div class="btn-group">
                            <button type="button" class="btn btn-info dropdown-toggle" data-bs-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">
                            <a class="dropdown-item btn-success" href="php/admin.php/approve?ref_no=${user.ref_no}">Sent</a>
                            <a class="dropdown-item" href="php/admin.php/rejectwithdrawer?ref_no=${user.ref_no}">Reject</a>
                            <a class="dropdown-item" > </a>
                            <a class="dropdown-item" > </a>
                            <a class="dropdown-item" > </a>
                            <a class="dropdown-item" > </a>
                            <a class="dropdown-item" > </a>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        `;
    }

    placeholder.innerHTML = out;
})
